function ImagePreload( p_aImages, p_pfnPercent, p_pfnFinished )
{   // Call-back routines
    this.m_pfnPercent = p_pfnPercent;
    this.m_pfnFinished = p_pfnFinished;

    // Class Member Vars
    this.m_nLoaded = 0;
    this.m_nProcessed = 0;
    this.m_aImages = new Array;
    this.m_nICount = p_aImages.length;

    // Preload Array of Images
    for( var i = 0; i < p_aImages.length; i++ ) {
        //console.log(p_aImages[i].indexOf('nodata'));
        if (p_aImages[i].indexOf('nodata') != -1) p_aImages[i] = '/img/blank.png';
        this.Preload( p_aImages[i] );
    }
}

ImagePreload.prototype.Preload = function( p_oImage )
{   var oImage = new Image;
    this.m_aImages.push( oImage );

    oImage.onload = ImagePreload.prototype.OnLoad;
    oImage.onerror = ImagePreload.prototype.OnError;
    oImage.onabort = ImagePreload.prototype.OnAbort;

    oImage.oImagePreload = this;
    oImage.bLoaded = false;
    oImage.source = p_oImage;
    oImage.src = p_oImage;
}

ImagePreload.prototype.OnComplete = function()
{   this.m_nProcessed++;
    if ( this.m_nProcessed == this.m_nICount )
        this.m_pfnFinished();
    else
        this.m_pfnPercent( Math.round( (this.m_nProcessed / this.m_nICount) * 10 ) );
}

ImagePreload.prototype.OnLoad = function()
{   // 'this' pointer points to oImage Object
    this.bLoaded = true;
    this.oImagePreload.m_nLoaded++;
    this.oImagePreload.OnComplete();
}

ImagePreload.prototype.OnError = function()
{   // 'this' pointer points to oImage Object
    this.bError = true;
    this.oImagePreload.OnComplete();
}

ImagePreload.prototype.OnAbort = function()
{   // 'this' pointer points to oImage Object
    this.bAbort = true;
    this.oImagePreload.OnComplete();
}
