# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 10:58:21 2017

@author: ep
"""

"""      Imports      """

import datetime
import numpy as np
import os
import pandas as PD
from glob import glob

"""      Consts       """

rpath = os.path.join('src','160805_data')

""" Local Definitions """

files = glob(os.path.join(rpath,'*-*.txt'))

files = glob(os.path.join(rpath,'*-suma.txt'))


def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / N


"""   Data Loading    """

LS = []

for fpath in files:
    with open(fpath,'r') as f:
        LS.extend(f.readlines())

LS = [L.replace('\n','').split(',') for L in LS]
LS = [ [datetime.datetime.strptime(l[0],'%d.%m.%Y %H:%M:%S.%f'),float(l[1]),float(l[2])] for l in LS if len(l)>1]

Dates = [L[0] for L in LS]
D1 = [L[1] for L in LS]
D2 = [L[2] for L in LS]
"""    Processing     """


"""      Saving       """



""" """
#
