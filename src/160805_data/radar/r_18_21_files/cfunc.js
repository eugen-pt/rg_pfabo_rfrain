function setGMapsCookie(mesto_id)
{
var now = new Date();
fixDate(now);
now.setTime(now.getTime() +  7 * 24 * 60 * 60 * 1000);    
setCookie('SHMU_NWP_GMaps',mesto_id, now); 
}

function getGMapsCookie()
{
  var mesto_id = getCookie('SHMU_NWP_GMaps');
  return mesto_id;
}


function setTabCookie(idtab,tabno)
{
var now = new Date();
fixDate(now);
now.setTime(now.getTime() +  7 * 24 * 60 * 60 * 1000);    
setCookie(idtab,tabno, now); 
}

function getTabCookie(idtab)
{
  var tabno = getCookie(idtab);
  return tabno;
}
    
/* */

/*
   name - name of the cookie
   value - value of the cookie
   [expires] - expiration date of the cookie
     (defaults to end of current session)
   [path] - path for which the cookie is valid
     (defaults to path of calling document)
   [domain] - domain for which the cookie is valid
     (defaults to domain of calling document)
   [secure] - Boolean value indicating if the cookie transmission requires
     a secure transmission
   * an argument defaults when it is assigned null as a placeholder
   * a null placeholder is not required for trailing omitted arguments
*/

function setCookie(name, value, expires, path, domain, secure) {
  var curCookie = name + "=" + escape(value) +
      ((expires) ? "; expires=" + expires.toGMTString() : "") +
      ((path) ? "; path=" + path : "") +
      ((domain) ? "; domain=" + domain : "") +
      ((secure) ? "; secure" : "");
  document.cookie = curCookie;
}


/*
  name - name of the desired cookie
  return string containing value of specified cookie or null
  if cookie does not exist
*/

function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);
  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else
    begin += 2;
  var end = document.cookie.indexOf(";", begin);
  if (end == -1)
    end = dc.length;
  return unescape(dc.substring(begin + prefix.length, end));
}


/*
   name - name of the cookie
   [path] - path of the cookie (must be same as path used to create cookie)
   [domain] - domain of the cookie (must be same as domain used to
     create cookie)
   path and domain default if assigned null or omitted if no explicit
     argument proceeds
*/

function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    document.cookie = name + "=" +
    ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}

// date - any instance of the Date object
// * hand all instances of the Date object to this function for "repairs"

function fixDate(date) {
  var base = new Date(0);
  var skew = base.getTime();
  if (skew > 0)
    date.setTime(date.getTime() - skew);
}








/* --------------------- */

var menu = null;
function init_menu()
{
	menu = document.getElementById('menu');
	var li = menu.getElementsByTagName('li'), i = li.length;
	while (i--) li[i].onmouseover = showMenu;
	menu.onmouseout = timeout;
	menu.onmouseover = cleartimer;
}

var timer = null;
function timeout()
{
	timer = setTimeout('hideMenus(menu, null)', 500);

}

function cleartimer()
{
	if (timer)
	{
		clearTimeout(timer);
		timer = null;
	}
}

function showMenu()
{
	var ul = this.parentNode;
	while (ul)
	{
		if (ul.tagName.toLowerCase() == 'ul')
		{
			hideMenus(ul, this);
			break;
		}

		ul = ul.parentNode;
	}

	ul = this.firstChild;
	while (ul)
	{
		if (ul.nodeType == 1 && ul.tagName.toLowerCase() == 'ul')
		{
			ul.style.display = 'block';
			ul.style.visibility = ''; // necessary for IE
			break;
		}

		ul = ul.nextSibling;
	}
}

function hideMenus(level, skipli)
{
	var stack = [level], i = 0, li, j, el, tag;
	do
	{
		li = stack[i].childNodes, j = li.length;
		while (j--)
		{
			el = li[j];
			if (el.nodeType == 1 && el != skipli)
			{
				tag = el.tagName.toLowerCase();
				if (tag == 'li')
				{
					stack[i++] = el;
				}
				else if (tag == 'ul' && el.style.display == 'block')
				{
					stack[i++] = el;
					el.style.display = 'none';
					el.style.visibility = 'hidden'; // necessary for IE
				}
			}
		}
	}
	while (i--);
}



